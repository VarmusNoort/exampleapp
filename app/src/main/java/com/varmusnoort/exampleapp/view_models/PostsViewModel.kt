package com.varmusnoort.exampleapp.view_models

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.varmusnoort.exampleapp.model.Post
import com.varmusnoort.exampleapp.model.PostInfo
import com.varmusnoort.exampleapp.model.PostWithPostInfo
import com.varmusnoort.exampleapp.repository.PostRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PostsViewModel()  : ViewModel() {
    private lateinit var postRepository: PostRepository
    private var posts: LiveData<List<PostWithPostInfo>>? = null

    @Inject constructor(
        postRepository: PostRepository
    ) : this() {
        this.postRepository = postRepository
    }

    fun getPosts() : LiveData<List<PostWithPostInfo>> {
        if (posts == null) {
            posts = postRepository.getPosts()
        }
        return posts as LiveData<List<PostWithPostInfo>>
    }

    fun getPost(id : Long) : LiveData<PostWithPostInfo> {
        return postRepository.getPost(id)
    }

    fun addPostToFavorites(postWithPostInfo : PostWithPostInfo) {
        postWithPostInfo.postInfo?.let {
            it.isFavorite = true
            postRepository.updatePostInfo(it)
        }
    }

    fun removePostFromFavorites(postWithPostInfo : PostWithPostInfo) {
        val postInfo = postWithPostInfo.postInfo
        if (postInfo != null) {
            postInfo.isFavorite = false
            postRepository.updatePostInfo(postInfo)
        }
    }
}