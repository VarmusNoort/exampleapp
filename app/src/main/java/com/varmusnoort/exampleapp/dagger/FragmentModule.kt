package com.varmusnoort.exampleapp.dagger

import com.varmusnoort.exampleapp.ui.fragments.PostDetailsFragment
import com.varmusnoort.exampleapp.ui.fragments.PostListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributePostListFragment(): PostListFragment

    @ContributesAndroidInjector
    abstract fun contributePostDetailsFragment(): PostDetailsFragment
}