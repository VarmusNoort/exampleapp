package com.varmusnoort.exampleapp.dagger

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.varmusnoort.exampleapp.dagger.common.ViewModelFactory
import com.varmusnoort.exampleapp.dagger.common.ViewModelKey
import com.varmusnoort.exampleapp.view_models.PostsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(PostsViewModel::class)
    protected abstract fun postListViewModel(postsViewModel: PostsViewModel): ViewModel

}