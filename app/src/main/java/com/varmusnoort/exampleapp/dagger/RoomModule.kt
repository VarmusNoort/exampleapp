package com.varmusnoort.exampleapp.dagger

import android.app.Application
import androidx.room.Room
import com.varmusnoort.exampleapp.repository.network.NetworkService
import com.varmusnoort.exampleapp.repository.PostDatabase
import com.varmusnoort.exampleapp.repository.dao.PostDao
import com.varmusnoort.exampleapp.repository.dao.PostInfoDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class RoomModule {

    private var postDatabase: PostDatabase? = null
    private var networkService : NetworkService? = null

    @Singleton
    @Provides
    fun providesPostDatabase(mApplication: Application): PostDatabase {
        if (postDatabase == null) {
            postDatabase = Room
                .databaseBuilder(mApplication, PostDatabase::class.java, "posts.db")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build()
        }
        return postDatabase!!
    }

    @Singleton
    @Provides
    fun providesNetworkService(): NetworkService {
        if (networkService == null) {
            networkService = NetworkService()
        }
        return networkService!!
    }

    @Singleton
    @Provides
    fun providesPostDao(postDatabase: PostDatabase): PostDao {
        return postDatabase.postDao()
    }

    @Singleton
    @Provides
    fun providesPostInfoDao(postDatabase: PostDatabase): PostInfoDao {
        return postDatabase.postInfoDao()
    }
}