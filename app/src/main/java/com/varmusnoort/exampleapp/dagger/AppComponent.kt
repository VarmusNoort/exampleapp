package com.varmusnoort.exampleapp.dagger

import android.app.Application
import com.varmusnoort.exampleapp.MainActivity
import com.varmusnoort.exampleapp.dagger.common.AppController
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Component(
    modules = [
        RoomModule::class,
        ActivityModule::class,
        AndroidInjectionModule::class]
)
@Singleton
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    fun inject(mainActivity: MainActivity)
    fun inject(appController: AppController)
}