package com.varmusnoort.exampleapp.dagger

import com.varmusnoort.exampleapp.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [
        FragmentModule::class,
        ViewModelModule::class
    ])
    abstract fun contributeMainActivity(): MainActivity

}