package com.varmusnoort.exampleapp.dagger.common

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule {

    private lateinit var mApplication: Application

    fun AppModule(application: Application) {
        mApplication = application
    }

    @Provides
    @Singleton
    fun providesApplication(): Application {
        return mApplication
    }
}