package com.varmusnoort.exampleapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "post_info")
class PostInfo(@PrimaryKey var postId: Long = 0, var isFavorite: Boolean)