package com.varmusnoort.exampleapp.model

import androidx.room.Embedded
import androidx.room.Relation

class PostWithPostInfo {

    @Embedded
    lateinit var post : Post

    @Relation(parentColumn = "id", entityColumn = "postId")
    var postInfo : PostInfo? = null
}