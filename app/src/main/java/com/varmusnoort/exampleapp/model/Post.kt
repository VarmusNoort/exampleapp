package com.varmusnoort.exampleapp.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "post")
data class Post(
    var title: String? = null,
    @SerializedName("body")
    var text: String? = null) {

    @PrimaryKey
    var id: Long = 0
    @SerializedName("user_id")
    var userId: Long = 0
}