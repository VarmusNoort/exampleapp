package com.varmusnoort.exampleapp.ui.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.varmusnoort.exampleapp.R
import com.varmusnoort.exampleapp.dagger.common.InjectionFragment
import com.varmusnoort.exampleapp.databinding.FragmentPostDetailsBinding
import com.varmusnoort.exampleapp.model.Post
import com.varmusnoort.exampleapp.model.PostWithPostInfo
import com.varmusnoort.exampleapp.view_models.PostsViewModel


class PostDetailsFragment : InjectionFragment() {

    private lateinit var postsViewModel: PostsViewModel

    private val args: PostDetailsFragmentArgs by navArgs()

    private var tvTitle : TextView? = null
    private var tvText : TextView? = null
    private var ivIcon : ImageView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postsViewModel = injectViewModel(viewModelFactory)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentPostDetailsBinding.inflate(layoutInflater)
        tvTitle = binding.tvTitle
        tvText = binding.tvText
        ivIcon = binding.ivIcon

        val postId = args.postId
        postsViewModel.getPost(postId).observe(viewLifecycleOwner, Observer { postWithPostInfo ->
            val title = postWithPostInfo.post.title ?: ""
            val text = postWithPostInfo.post.text ?: ""
            tvTitle?.text = title.trim().capitalize()
            tvText?.text = text.trim().capitalize()

            val postInfo = postWithPostInfo.postInfo
            val icon = if (postInfo != null && postInfo.isFavorite) {
                R.drawable.ic_post_favorite_star_active
            } else {
                R.drawable.ic_post_favorite_star_inactive
            }
            ivIcon?.setImageResource(icon)

            ivIcon?.setOnClickListener {
                val postInfo = postWithPostInfo.postInfo
                if(postInfo != null) {
                    if (postInfo.isFavorite) {
                        showRemovePostFromFavoritesDialog(postWithPostInfo)
                    } else {
                        postsViewModel.addPostToFavorites(postWithPostInfo)
                    }
                }
            }
        })

        return binding.root
    }

    private fun showRemovePostFromFavoritesDialog(postWithPostInfo : PostWithPostInfo) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
        builder.setMessage(R.string.dialog_remove_post_from_favorites)
            .setPositiveButton(R.string.dialog_remove_post_from_favorites_yes) { _, _ ->
                postsViewModel.removePostFromFavorites(postWithPostInfo)
            }
            .setNegativeButton(R.string.dialog_remove_post_from_favorites_cancel) { _, _ ->
                // User cancelled the dialog
            }
        builder.create().show()
    }
}