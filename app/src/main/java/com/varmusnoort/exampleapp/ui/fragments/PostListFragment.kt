package com.varmusnoort.exampleapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.varmusnoort.exampleapp.dagger.common.InjectionFragment
import com.varmusnoort.exampleapp.databinding.FragmentPostListBinding
import com.varmusnoort.exampleapp.model.Post
import com.varmusnoort.exampleapp.model.PostWithPostInfo
import com.varmusnoort.exampleapp.ui.adapters.PostListRecycleAdapter
import com.varmusnoort.exampleapp.view_models.PostsViewModel


class PostListFragment : InjectionFragment() {

    private lateinit var postsViewModel: PostsViewModel

    private var recyclerView : RecyclerView? = null

    private var postListRecycleAdapter : PostListRecycleAdapter? = null
    private var onPostClickedListener = object : PostListRecycleAdapter.OnPostClickedListener {
        override fun onPostClicked(postWithPostInfo: PostWithPostInfo) {
            val action = PostListFragmentDirections.actionPostListFragmentToPostDetailsFragment(postWithPostInfo.post.id)
            findNavController().navigate(action)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postsViewModel = injectViewModel(viewModelFactory)
        postListRecycleAdapter = PostListRecycleAdapter(requireContext())
        postListRecycleAdapter?.onPostClickedListener = onPostClickedListener
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentPostListBinding.inflate(layoutInflater)
        recyclerView = binding.recyclerView

        recyclerView?.adapter = postListRecycleAdapter
        recyclerView?.layoutManager = LinearLayoutManager(requireContext())

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        postsViewModel.getPosts().observe(viewLifecycleOwner, Observer {
            postListRecycleAdapter?.setItems(it)
        })
    }
}