package com.varmusnoort.exampleapp.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.varmusnoort.exampleapp.R
import com.varmusnoort.exampleapp.model.Post
import com.varmusnoort.exampleapp.model.PostWithPostInfo
import com.varmusnoort.exampleapp.utils.CommonUtils.forceEllipsize

class PostListRecycleAdapter(context: Context) : RecyclerView.Adapter<PostListRecycleAdapter.BaseViewHolder>() {

    private val postsViewHolderFactory = PostsViewHolderFactory(context)

    private var items: ArrayList<ListItem> = ArrayList()

    var onPostClickedListener : OnPostClickedListener? = null

    fun setItems(posts : List<PostWithPostInfo>) {
        items.clear()
        for (post in posts) {
            items.add(TextItem(post))
        }
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        when(ItemType.getByValue(holder.itemViewType)) {
            ItemType.TEXT -> {
                val viewHolderPostText = holder as ViewHolderPostText
                val textItem = items[position] as TextItem
                val title = textItem.post.post.title ?: ""
                val text = textItem.post.post.text ?: ""
                viewHolderPostText.tvTitle.text = title.trim().capitalize()
                viewHolderPostText.tvText.text = text.trim().capitalize().forceEllipsize(100)

                val postInfo = textItem.post.postInfo
                val icon = if (postInfo != null && postInfo.isFavorite) {
                    R.drawable.ic_post_favorite_star_active
                } else {
                    R.drawable.ic_post_favorite_star_inactive
                }
                viewHolderPostText.ivIcon.setImageResource(icon)

                viewHolderPostText.container.setOnClickListener {
                    onPostClickedListener?.onPostClicked(textItem.post)
                }
            }
        }
    }


    private class TextItem(var post: PostWithPostInfo) : ListItem {
        override var type: ItemType = ItemType.TEXT
    }

    class ViewHolderPostText(itemView: View) : BaseViewHolder(itemView) {
        var container: ConstraintLayout = itemView.findViewById<View>(R.id.container) as ConstraintLayout
        var tvTitle: TextView = itemView.findViewById<View>(R.id.tvTitle) as TextView
        var tvText: TextView = itemView.findViewById<View>(R.id.tvText) as TextView
        var ivIcon: ImageView = itemView.findViewById<View>(R.id.ivIcon) as ImageView
    }

    enum class ItemType(val type: Int) {
        TEXT(1);
        companion object {
            private val values = values();
            fun getByValue(type: Int) = values.first { it.type == type }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val itemType: ItemType = ItemType.getByValue(viewType)
        return postsViewHolderFactory.createViewHolder(parent, itemType)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].type.type
    }

    private interface ListItem {
        var type: ItemType
    }

    open class BaseViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView)

    private inner class PostsViewHolderFactory(context: Context) {
        private val mInflater: LayoutInflater? = LayoutInflater.from(context)

        fun createViewHolder(parent : ViewGroup, itemType : ItemType) : BaseViewHolder {
            val layout: Int
            val convertView: View
            when(itemType) {
                ItemType.TEXT -> {
                    layout = (R.layout.post_text)
                    convertView = mInflater!!.inflate(layout, parent, false)
                    return ViewHolderPostText(convertView)
                }
            }
        }
    }

    interface OnPostClickedListener {
        fun onPostClicked(post: PostWithPostInfo)
    }

}