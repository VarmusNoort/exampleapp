package com.varmusnoort.exampleapp.repository.dao

import androidx.room.*

interface BaseDao<T> {
    @Insert
    fun insert(obj: T) : Long

    @Insert
    fun insert(vararg obj: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(objects: List<T>) : List<Long>

    @Update
    fun update(obj: T)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateAll(objects: List<T>)

    @Delete
    fun delete(obj: T)

}