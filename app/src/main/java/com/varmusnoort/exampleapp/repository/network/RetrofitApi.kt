package com.varmusnoort.exampleapp.repository.network

import com.varmusnoort.exampleapp.model.Post
import retrofit2.Call
import retrofit2.http.GET

interface RetrofitApi {

    @GET("posts")
    fun getPosts(): Call<List<Post>>?

}