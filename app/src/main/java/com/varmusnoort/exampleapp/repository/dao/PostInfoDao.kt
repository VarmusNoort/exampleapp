package com.varmusnoort.exampleapp.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.varmusnoort.exampleapp.model.PostInfo


@Dao
interface PostInfoDao : BaseDao<PostInfo> {
    @get:Query("SELECT * FROM post_info")
    val all: LiveData<List<PostInfo>>
    @get:Query("SELECT * FROM post_info")
    val allSync: List<PostInfo>

    @Query("SELECT * FROM post_info WHERE postId = :id")
    fun getById(id: Long): LiveData<PostInfo>
    @Query("SELECT * FROM post_info WHERE postId = :id")
    fun getSyncById(id: Long): PostInfo

    @Query("DELETE FROM post_info")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertWithoutReplacement(postInfo: PostInfo) : Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAllWithoutReplacement(objects: List<PostInfo>) : List<Long>

}