package com.varmusnoort.exampleapp.repository

import androidx.lifecycle.LiveData
import com.varmusnoort.exampleapp.model.Post
import com.varmusnoort.exampleapp.model.PostInfo
import com.varmusnoort.exampleapp.model.PostWithPostInfo
import com.varmusnoort.exampleapp.repository.dao.PostDao
import com.varmusnoort.exampleapp.repository.dao.PostInfoDao
import com.varmusnoort.exampleapp.repository.network.NetworkService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class PostRepository @Inject constructor(
    private val postDao: PostDao,
    private val postInfoDao: PostInfoDao,
    private val networkService: NetworkService
)  {

    //
    // Post
    //
    fun getPosts(): LiveData<List<PostWithPostInfo>> {
        requestPosts()
        return postDao.all
    }

    fun getPost(id: Long): LiveData<PostWithPostInfo> {
        return postDao.getById(id)
    }
    fun addPost(post : Post) : Long {
        postInfoDao.insertWithoutReplacement(PostInfo(post.id, false))
        return postDao.insert(post)
    }
    fun addPosts(posts : List<Post>) : List<Long> {
        val postInfos: ArrayList<PostInfo> = ArrayList()
        for (post in posts) {
            postInfos.add(PostInfo(post.id, false))
        }
        postInfoDao.insertAllWithoutReplacement(postInfos)
        return postDao.insertAll(posts)
    }
    fun updatePost(post : Post) {
        postDao.update(post)
    }
    fun deleteAllPosts() {
        postDao.deleteAll()
    }

    //
    // PostInfo
    //
    fun getPostInfos(): LiveData<List<PostInfo>> {
        return postInfoDao.all
    }

    fun getPostInfo(id: Long): LiveData<PostInfo> {
        return postInfoDao.getById(id)
    }
    fun addPostInfo(postInfo : PostInfo) : Long {
        return postInfoDao.insert(postInfo)
    }
    fun addPostInfos(postInfos : List<PostInfo>) : List<Long> {
        return postInfoDao.insertAll(postInfos)
    }
    fun updatePostInfo(postInfo : PostInfo) {
        postInfoDao.update(postInfo)
    }
    fun deleteAllPostInfos() {
        postInfoDao.deleteAll()
    }

    private fun requestPosts() {
        val call = networkService.getNetworkRequester().getPosts()
        call?.enqueue(object : Callback<List<Post>> {
            override fun onResponse(
                call: Call<List<Post>>,
                response: Response<List<Post>>
            ) {
                if (response.isSuccessful) {
                    val posts: List<Post>? = response.body()
                    if (posts != null) {
                        postDao.saveNew(posts)

                        val postInfos: ArrayList<PostInfo> = ArrayList()
                        for (post in posts) {
                            postInfos.add(PostInfo(post.id, false))
                        }
                        postInfoDao.insertAllWithoutReplacement(postInfos)
                    }
                } else {
                    println()
                }
            }
            override fun onFailure(
                call: Call<List<Post>>,
                t: Throwable
            ) {
                println()
            }
        })
    }
}