package com.varmusnoort.exampleapp.repository

import androidx.room.Database
import androidx.room.RoomDatabase
import com.varmusnoort.exampleapp.model.Post
import com.varmusnoort.exampleapp.model.PostInfo
import com.varmusnoort.exampleapp.repository.dao.PostDao
import com.varmusnoort.exampleapp.repository.dao.PostInfoDao

@Database(entities = [Post::class, PostInfo::class], version = 11)
abstract class PostDatabase : RoomDatabase() {

    abstract fun postDao(): PostDao

    abstract fun postInfoDao(): PostInfoDao

}