package com.varmusnoort.exampleapp.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.varmusnoort.exampleapp.model.Post
import com.varmusnoort.exampleapp.model.PostWithPostInfo

@Dao
interface PostDao : BaseDao<Post> {
    @get:Query("SELECT * FROM post")
    val all: LiveData<List<PostWithPostInfo>>
    @get:Query("SELECT * FROM post")
    val allSync: List<PostWithPostInfo>

    @Query("SELECT * FROM post WHERE id = :id")
    fun getById(id: Long): LiveData<PostWithPostInfo>
    @Query("SELECT * FROM post WHERE id = :id")
    fun getSyncById(id: Long): PostWithPostInfo

    @Query("DELETE FROM post")
    fun deleteAll()


    @Transaction
    fun saveNew(newPosts: List<Post>) {
        deleteAll()
        insertAll(newPosts)
    }

}