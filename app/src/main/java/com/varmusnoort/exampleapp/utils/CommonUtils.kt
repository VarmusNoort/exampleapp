package com.varmusnoort.exampleapp.utils

import android.util.LongSparseArray
import java.util.ArrayList


object CommonUtils {

    // There are some problems with TextView ellipsis combined with maxLength (and without maxLines)
    @JvmStatic
    fun String.forceEllipsize(maxLength : Int) : String {
        return if (this.length < maxLength) {
            this
        } else {
            this.substring(0, maxLength).trim() + '…'
        }
    }

}